package redis

import (
	"context"
	"errors"
	"fmt"
	recursive_deep_hash "github.com/panospet/recursive-deep-hash"
	"github.com/redis/go-redis/v9"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoregistry"
	"google.golang.org/protobuf/types/known/anypb"
	"time"
)

type Redis struct {
	Client          *redis.Client
	methodsForCache map[string]int
}

func NewRedis(client *redis.Client) *Redis {
	return &Redis{
		Client: client,
		// TODO read from config
		// TODO use make(map[string]int, x) in order to create sized map
		// TODO fill the map after understanding why it breaks the logic
		methodsForCache: map[string]int{
			//"/grades.GradesService/GetGradesWithEntities": 60,
			//"/grades.GradesService/GetGrades":             60,
		},
	}
}

func (redis *Redis) SetCache(ctx context.Context, resp interface{}, key string, expTime time.Duration) error {
	message, ok := resp.(proto.Message)
	if !ok {
		return errors.New("can not use resp as proto message")
	}

	// use anypb.Any as a layer for store message because it contains message's type.
	anyMessage, err := anypb.New(message)
	if err != nil {
		fmt.Println(fmt.Sprint("can not create anypb from message: ", err))
		return err
	}

	data, err := proto.Marshal(anyMessage)
	if err != nil {
		fmt.Println(fmt.Sprint("can not marshal message: ", err))
		return err
	}

	set, err := redis.Client.SetNX(ctx, key, data, expTime).Result()
	if err != nil || !set {
		fmt.Println(fmt.Sprint("can not set cache: ", err))
		return err
	}
	return nil
}

func (redis *Redis) GetCache(ctx context.Context, key string) (proto.Message, error) {
	val, err := redis.Client.Get(ctx, key).Bytes()
	if err != nil {
		fmt.Println(fmt.Sprint("can not get cache from redis: ", err))
		return nil, err
	}

	// use anypb.Any to store data in cache in the way it is enabled to use as responses for rpc determined in proto file
	var cacheStruct anypb.Any
	if err = proto.Unmarshal(val, &cacheStruct); err != nil {
		fmt.Println(fmt.Sprint("can not marshal message: ", err))
		return nil, err
	}

	// create proto.Message that is real response type for our request (saved and taken from cache anypb.Any)
	t, err := protoregistry.GlobalTypes.FindMessageByName(cacheStruct.MessageName())
	if err != nil {
		fmt.Println(fmt.Sprint("can not find the type of message: ", err))
		return nil, err
	}
	m := t.New().Interface()

	// unmarshal bytes array in structure of certain type (from previous step)
	if err = proto.Unmarshal(cacheStruct.Value, m); err != nil {
		fmt.Println(fmt.Sprint("can not marshal message: ", err))
		return nil, err
	}

	return m, nil
}

func (redis *Redis) GenerateHash(resp interface{}, userUuid, fullMethod string) (string, error) {
	hs := HashStruct{
		Resp:       resp,
		UuidUser:   userUuid,
		FullMethod: fullMethod,
	}
	return recursive_deep_hash.ConstructHash(hs)
}

// HashStruct is helpful struct for hash key creating.
// All fields that you want to cache must be public (ask recursive_deep_hash developer why he made it work like that).
type HashStruct struct {
	Resp       interface{}
	UuidUser   string
	FullMethod string
}

// GetExpiredTimeForMethod returns the expired time of cache for the method. If the method is not allowed to be cached the function returns 0.
func (redis *Redis) GetExpiredTimeForMethod(method string) int {
	for name, expTime := range redis.methodsForCache {
		if name == method {
			return expTime
		}
	}
	return 0
}
