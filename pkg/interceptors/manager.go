package interceptors

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"study-plans-service/pkg/logger"
	"study-plans-service/pkg/redis"
	"time"
)

type InterceptorManager interface {
	Logger(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (resp interface{}, err error)
	Cache(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (resp interface{}, err error)
}

// InterceptorManager struct
type interceptorManager struct {
	logger logger.Logger
	redis  *redis.Redis
}

// NewInterceptorManager InterceptorManager constructor
func NewInterceptorManager(logger logger.Logger, redis *redis.Redis) *interceptorManager {
	return &interceptorManager{
		logger: logger,
		redis:  redis,
	}
}

// Logger Interceptor
func (im *interceptorManager) Logger(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {
	start := time.Now()
	md, _ := metadata.FromIncomingContext(ctx)
	reply, err := handler(ctx, req)
	im.logger.GrpcMiddlewareAccessLogger(info.FullMethod, time.Since(start), md, err)
	return reply, err
}

func (im *interceptorManager) Cache(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {
	// check if the method is allowed to be cached and get expired time in case it is allowed
	expTime := im.redis.GetExpiredTimeForMethod(info.FullMethod)
	if expTime == 0 {
		return handler(ctx, req)
	}

	// get user-uuid from metadata
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok || len(md.Get("user-uuid")) < 1 {
		return handler(ctx, req)
	}
	userUuid := md.Get("user-uuid")[0]

	// generate hash to use it as key in redis
	hash, err := im.redis.GenerateHash(req, userUuid, info.FullMethod)
	if err != nil {
		fmt.Printf("can not generate hash key for user %s and method %s: %s\n", userUuid, info.FullMethod, err)
	}

	// check if there is a cache for such request for this user
	cache, err := im.redis.GetCache(ctx, hash)
	// in case there is a cache return the cache to client
	if err == nil {
		return cache, nil
	}

	// handle request
	resp, err = handler(ctx, req)

	// caching response from core
	if err = im.redis.SetCache(ctx, resp, hash, time.Second*time.Duration(expTime)); err != nil {
		im.logger.Warnf("Can not set cache for user %s and method %s", userUuid, info.FullMethod)
	}

	return
}
