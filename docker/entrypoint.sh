#!/usr/bin/env sh

#set environment variable
export GOOSE_DBSTRING="host=${POSTGRES_HOST} port=6432 user=${POSTGRES_USER} password=${POSTGRES_PASSWORD} dbname=${POSTGRES_DB} sslmode=require"
export GOOSE_DRIVER=postgres

#run
cd migrations && goose up
cd .. && ./service
