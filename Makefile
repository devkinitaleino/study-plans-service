# ==============================================================================
# Microservice
run:
	go run cmd/service/main.go

linters:
	golangci-lint run

# ==============================================================================
# Proto
gen:
	protoc -I proto \
    --go-grpc_opt=require_unimplemented_servers=false --go_out=./proto --go-grpc_out=./proto \
    --validate_out="lang=go:./proto" proto/plans.proto

gen_groups_client:
	protoc -I internal/client/services \
    --go_opt=paths=source_relative  \
    --go-grpc_opt=paths=source_relative \
    --validate_opt=paths=source_relative \
    --go-grpc_opt=require_unimplemented_servers=false \
    --go_out=./internal/client/services \
    --go-grpc_out=./internal/client/services \
    --validate_out="lang=go:./internal/client/services"  \
    internal/client/services/group_service/group_service.proto
# ==============================================================================
# Docker
build:
	make gen && make docker

local:
	cd docker \
	&& docker-compose -f docker-compose.local.yml up --build

docker:
	cd docker \
	&& docker-compose up --build

db:
	cd docker \
	&& docker-compose up -d db

migration_up:
	cd docker \
	&& docker-compose exec go sh -c "cd migrations && goose up"

migration_down:
	cd docker \
	&& docker-compose exec go sh -c "cd migrations && goose down"
