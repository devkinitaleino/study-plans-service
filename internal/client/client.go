package client

import (
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"study-plans-service/config"
	"study-plans-service/internal/client/services/auth"
	groupService "study-plans-service/internal/client/services/group_service"
	"study-plans-service/pkg/logger"
)

type Client struct {
	log         logger.Logger
	groupClient groupService.GroupServiceClient
	authClient  auth.AuthServiceClient
}

func NewClient(cfg *config.Config, log logger.Logger) *Client {
	connGroups, err := grpc.Dial(
		cfg.Services.Groups,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
	)
	if err != nil {
		log.Warn(err)
	}

	connAuth, err := grpc.Dial(
		cfg.Services.Auth,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
	)
	if err != nil {
		log.Warn(err)
	}

	return &Client{
		log:         log,
		authClient:  auth.NewAuthServiceClient(connAuth),
		groupClient: groupService.NewGroupServiceClient(connGroups),
	}
}
