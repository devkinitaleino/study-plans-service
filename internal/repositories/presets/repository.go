package presets

import (
	"context"
	"study-plans-service/internal/domain"
)

type Repository interface {
	Create(ctx context.Context, pres *domain.Presets) error
	Delete(ctx context.Context, uuid domain.UuidString) error
	Update(ctx context.Context, pres *domain.Presets) error
	Get(ctx context.Context, uuids []string) ([]domain.Presets, error)

	CreateCrossPresetsGroups(ctx context.Context, uuidPreset string, uuidGroup string) error
	CreateCrossDisciplinesPresets(ctx context.Context, uuidPreset string, uuidDiscipline string) error

	DeleteCrossPresetsGroups(ctx context.Context, uuid domain.UuidString) error
	DeleteCrossDisciplinesPresets(ctx context.Context, uuid domain.UuidString) error
}
