package presets

import (
	"context"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.opentelemetry.io/otel"
	"strings"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/repositories"
	"study-plans-service/pkg/logger"
)

type RepositoryPostgre struct {
	pool   *pgxpool.Pool
	logger logger.Logger
}

const (
	CreatePresetQuery                  = `INSERT INTO presets (uuid, name) VALUES ($1,$2) RETURNING created_at, updated_at;`
	CreateCrossPresetsGroupsQuery      = `INSERT INTO cross_presets_groups (uuid, uuid_preset, uuid_group) VALUES ($1, $2, $3);`
	CreateCrossDisciplinesPresetsQuery = `INSERT INTO cross_disciplines_presets (uuid, uuid_preset, uuid_discipline) VALUES ($1, $2, $3);`
	DeletePresetQuery                  = `UPDATE presets SET deleted_at = now() WHERE uuid = $1;`
	DeleteCrossPresetsGroupsQuery      = `DELETE FROM cross_presets_groups WHERE uuid_preset = $1;`
	DeleteCrossDisciplinesPresetsQuery = `DELETE FROM cross_disciplines_presets WHERE uuid_preset = $1;`
	UpdatePresetQuery                  = `UPDATE presets SET name = $2 WHERE uuid = $1 RETURNING created_at, updated_at;`
	GetPresetsByUuidsQuery             = `SELECT uuid, name, created_at, updated_at FROM presets WHERE deleted_at IS NULL AND uuid IN `
	GetAllPresetsQuery                 = `SELECT uuid, name, created_at, updated_at FROM presets WHERE deleted_at IS NULL;`
)

func NewRepository(pool *pgxpool.Pool, logger logger.Logger) Repository {
	return &RepositoryPostgre{
		pool:   pool,
		logger: logger,
	}
}

func (repo *RepositoryPostgre) Create(ctx context.Context, pres *domain.Presets) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Presets")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		CreatePresetQuery,
		pres.Uuid,
		pres.Name,
	).Scan(
		&pres.CreatedAt,
		&pres.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("Can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossPresetsGroups(ctx context.Context, uuidPreset string, uuidGroup string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Presets-Groups")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossPresetsGroupsQuery,
		uuid.New().String(),
		uuidPreset,
		uuidGroup,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossDisciplinesPresets(ctx context.Context, uuidPreset string, uuidDiscipline string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Disciplines-Presets")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossDisciplinesPresetsQuery,
		uuid.New().String(),
		uuidPreset,
		uuidDiscipline,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Delete(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Preset")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeletePresetQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossPresetsGroups(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Presets-Groups")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossPresetsGroupsQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossDisciplinesPresets(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Presets-Groups")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossDisciplinesPresetsQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Update(ctx context.Context, pres *domain.Presets) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Update Preset")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		UpdatePresetQuery,
		pres.Uuid,
		pres.Name,
	).Scan(
		&pres.CreatedAt,
		&pres.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("Can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Get(ctx context.Context, uuids []string) ([]domain.Presets, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Get Presets")
	defer childSpan.End()

	query := GetPresetsByUuidsQuery + "('" + strings.Join(uuids, "','") + "')" + ";"
	if len(uuids) == 0 {
		query = GetAllPresetsQuery
	}

	rows, err := repo.pool.Query(ctx, query)
	if err != nil {
		repo.logger.Warnf("Query error: %s", err)
		return nil, err
	}
	defer rows.Close()

	var presets []domain.Presets
	for rows.Next() {
		var preset domain.Presets
		if err = rows.Scan(
			&preset.Uuid,
			&preset.Name,
			&preset.CreatedAt,
			&preset.UpdatedAt,
		); err != nil {
			repo.logger.Warnf("Scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}
		presets = append(presets, preset)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("Rows error %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}
	return presets, err
}
