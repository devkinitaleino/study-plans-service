package skills

import (
	"context"
	"github.com/google/uuid"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.opentelemetry.io/otel"
	"strings"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/repositories"
	"study-plans-service/pkg/logger"
)

type RepositoryPostgre struct {
	pool   *pgxpool.Pool
	logger logger.Logger
}

const (
	CreateSkillsGroupQuery                      = `INSERT INTO skills_groups (uuid, name) VALUES ($1, $2) RETURNING created_at, updated_at;`
	UpdateSkillsGroupQuery                      = `UPDATE skills_groups SET name = $1 WHERE uuid = $2 RETURNING created_at, updated_at;`
	DeleteSkillsGroupQuery                      = `UPDATE skills_groups SET deleted_at = now() WHERE uuid = $1;`
	CreateSkillsProgrammeQuery                  = `INSERT INTO skills_programmes (uuid, name) VALUES ($1, $2) RETURNING created_at, updated_at;`
	UpdateSkillsProgrammeQuery                  = `UPDATE skills_programmes SET name = $1 WHERE uuid = $2 RETURNING created_at, updated_at;`
	DeleteSkillsProgrammeQuery                  = `UPDATE skills_programmes SET deleted_at = now() WHERE uuid = $1;`
	CreateSkillQuery                            = `INSERT INTO skills (uuid, name, content, picture) VALUES ($1, $2, $3, $4) RETURNING created_at, updated_at;`
	UpdateSkillQuery                            = `UPDATE skills SET name = $1, content = $2, picture = $3 WHERE uuid = $4 RETURNING created_at, updated_at;`
	DeleteSkillQuery                            = `UPDATE skills SET deleted_at = now() WHERE uuid = $1;`
	CreateCrossSkillsDisciplinesQuery           = `INSERT INTO cross_skills_disciplines (uuid, uuid_skill, uuid_discipline) VALUES ($1, $2, $3);`
	CreateCrossSkillsGroupsQuery                = `INSERT INTO cross_skills_groups (uuid, uuid_skill, uuid_group) VALUES ($1, $2, $3);`
	CreateCrossSkillsProgrammesQuery            = `INSERT INTO cross_skills_programmes (uuid, uuid_skill, uuid_programme) VALUES ($1, $2, $3);`
	DeleteCrossSkillsDisciplinesQuery           = `DELETE FROM cross_skills_disciplines WHERE uuid_skill = $1;`
	DeleteCrossSkillsGroupsQuery                = `DELETE FROM cross_skills_groups WHERE uuid_skill = $1;`
	DeleteCrossSkillsProgrammesQuery            = `DELETE FROM cross_skills_programmes WHERE uuid_skill = $1;`
	DeleteCrossSkillsGroupsByGroupQuery         = `DELETE FROM cross_skills_groups WHERE uuid_group = $1;`
	DeleteCrossSkillsProgrammesByProgrammeQuery = `DELETE FROM cross_skills_programmes WHERE uuid_programme = $1;`
	GetGroupsByUuidsQuery                       = `SELECT uuid, name, created_at, updated_at FROM skills_groups WHERE deleted_at IS NULL AND uuid IN `
	GetAllGroupsQuery                           = `SELECT uuid, name, created_at, updated_at FROM skills_groups WHERE deleted_at IS NULL;`
	GetProgrammesByUuidsQuery                   = `SELECT uuid, name, created_at, updated_at FROM skills_programmes WHERE deleted_at IS NULL AND uuid IN `
	GetAllProgrammesQuery                       = `SELECT uuid, name, created_at, updated_at FROM skills_programmes WHERE deleted_at IS NULL;`
	GetSkillsByUuidsQuery                       = `SELECT s.uuid, s.name, s.content, s.picture, s.created_at, s.updated_at, sg.uuid, sg.name, sp.uuid, sp.name
													FROM skills s
													LEFT JOIN cross_skills_groups csg on s.uuid = csg.uuid_skill
													LEFT JOIN skills_groups sg on sg.uuid = csg.uuid_group
													LEFT JOIN cross_skills_programmes csp on s.uuid = csp.uuid_skill
													LEFT JOIN skills_programmes sp on sp.uuid = csp.uuid_programme
													WHERE s.deleted_at IS NULL AND uuid IN `
	GetAllSkillsQuery = `SELECT s.uuid, s.name, s.content, s.picture, s.created_at, s.updated_at, sg.uuid, sg.name, sp.uuid, sp.name
													FROM skills s
													LEFT JOIN cross_skills_groups csg on s.uuid = csg.uuid_skill
													LEFT JOIN skills_groups sg on sg.uuid = csg.uuid_group
													LEFT JOIN cross_skills_programmes csp on s.uuid = csp.uuid_skill
													LEFT JOIN skills_programmes sp on sp.uuid = csp.uuid_programme
													WHERE s.deleted_at IS NULL;`
)

func NewRepository(pool *pgxpool.Pool, logger logger.Logger) Repository {
	return &RepositoryPostgre{
		pool:   pool,
		logger: logger,
	}
}

func (repo *RepositoryPostgre) Create(ctx context.Context, skill *domain.Skill) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Skill")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		CreateSkillQuery,
		skill.Uuid,
		skill.Name,
		skill.Content,
		skill.Picture,
	).Scan(
		&skill.CreatedAt,
		&skill.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Update(ctx context.Context, skill *domain.Skill) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Update Skill")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		UpdateSkillQuery,
		skill.Name,
		skill.Content,
		skill.Picture,
		skill.Uuid,
	).Scan(
		&skill.CreatedAt,
		&skill.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Delete(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Skill")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteSkillQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateGroup(ctx context.Context, skillsGroup *domain.SkillsGroup) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create SkillsGroup")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		CreateSkillsGroupQuery,
		skillsGroup.Uuid,
		skillsGroup.Name,
	).Scan(
		&skillsGroup.CreatedAt,
		&skillsGroup.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) UpdateGroup(ctx context.Context, skillsGroup *domain.SkillsGroup) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Update SkillsGroup")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		UpdateSkillsGroupQuery,
		skillsGroup.Name,
		skillsGroup.Uuid,
	).Scan(
		&skillsGroup.CreatedAt,
		&skillsGroup.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteGroup(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete SkillsGroup")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteSkillsGroupQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateProgramme(ctx context.Context, skillsProgramme *domain.SkillsProgramme) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create SkillsProgramme")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		CreateSkillsProgrammeQuery,
		skillsProgramme.Uuid,
		skillsProgramme.Name,
	).Scan(
		&skillsProgramme.CreatedAt,
		&skillsProgramme.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) UpdateProgramme(ctx context.Context, skillsProgramme *domain.SkillsProgramme) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Update SkillsProgramme")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		UpdateSkillsProgrammeQuery,
		skillsProgramme.Name,
		skillsProgramme.Uuid,
	).Scan(
		&skillsProgramme.CreatedAt,
		&skillsProgramme.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteProgramme(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete SkillsProgramme")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteSkillsProgrammeQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossSkillsDisciplines(ctx context.Context, uuidSkill, uuidDiscipline string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Skills-Disciplines")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossSkillsDisciplinesQuery,
		uuid.New().String(),
		uuidSkill,
		uuidDiscipline,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossSkillsGroups(ctx context.Context, uuidSkill, uuidGroup string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Skills-Groups")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossSkillsGroupsQuery,
		uuid.New().String(),
		uuidSkill,
		uuidGroup,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossSkillsProgrammes(ctx context.Context, uuidSkill, uuidProgramme string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Skills-Programmes")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossSkillsProgrammesQuery,
		uuid.New().String(),
		uuidSkill,
		uuidProgramme,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossSkillsDisciplines(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Skills-Disciplines")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossSkillsDisciplinesQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossSkillsGroups(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Skills-Groups")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossSkillsGroupsQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossSkillsProgrammes(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Skills-Programmes")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossSkillsProgrammesQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossSkillsGroupsByGroup(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Skills-Groups by group")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossSkillsGroupsByGroupQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossSkillsProgrammesByProgramme(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Skills-Programmes by programme")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossSkillsProgrammesByProgrammeQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) GetGroups(ctx context.Context, uuids []string) ([]domain.SkillsGroup, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Get Groups")
	defer childSpan.End()

	query := GetGroupsByUuidsQuery + "('" + strings.Join(uuids, "','") + "')" + ";"
	if len(uuids) == 0 {
		query = GetAllGroupsQuery
	}

	rows, err := repo.pool.Query(ctx, query)
	if err != nil {
		repo.logger.Warnf("Query error: %s", err)
		return nil, err
	}
	defer rows.Close()

	var groups []domain.SkillsGroup
	for rows.Next() {
		var group domain.SkillsGroup
		if err = rows.Scan(
			&group.Uuid,
			&group.Name,
			&group.CreatedAt,
			&group.UpdatedAt,
		); err != nil {
			repo.logger.Warnf("Scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}
		groups = append(groups, group)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("Rows error %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}
	return groups, err
}

func (repo *RepositoryPostgre) GetProgrammes(ctx context.Context, uuids []string) ([]domain.SkillsProgramme, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Get Programmes")
	defer childSpan.End()

	query := GetProgrammesByUuidsQuery + "('" + strings.Join(uuids, "','") + "')" + ";"
	if len(uuids) == 0 {
		query = GetAllProgrammesQuery
	}

	rows, err := repo.pool.Query(ctx, query)
	if err != nil {
		repo.logger.Warnf("Query error: %s", err)
		return nil, err
	}
	defer rows.Close()

	var programmes []domain.SkillsProgramme
	for rows.Next() {
		var programme domain.SkillsProgramme
		if err = rows.Scan(
			&programme.Uuid,
			&programme.Name,
			&programme.CreatedAt,
			&programme.UpdatedAt,
		); err != nil {
			repo.logger.Warnf("Scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}
		programmes = append(programmes, programme)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("Rows error %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}
	return programmes, err
}

func (repo *RepositoryPostgre) Get(ctx context.Context, uuids []string) ([]domain.Skill, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Get Skills")
	defer childSpan.End()

	query := GetSkillsByUuidsQuery + "('" + strings.Join(uuids, "','") + "')" + ";"
	if len(uuids) == 0 {
		query = GetAllSkillsQuery
	}

	rows, err := repo.pool.Query(ctx, query)
	if err != nil {
		repo.logger.Warnf("Query error: %s", err)
		return nil, err
	}
	defer rows.Close()

	var skills []domain.Skill
	for rows.Next() {
		var skill domain.Skill
		var skillsGroup SkillsGroup
		var skillsProgramme SkillsProgramme
		if err = rows.Scan(
			&skill.Uuid,
			&skill.Name,
			&skill.Content,
			&skill.Picture,
			&skill.CreatedAt,
			&skill.UpdatedAt,
			&skillsGroup.Uuid,
			&skillsGroup.Name,
			&skillsProgramme.Uuid,
			&skillsProgramme.Name,
		); err != nil {
			repo.logger.Warnf("Scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}
		if skillsGroup.Uuid.Status == pgtype.Present {
			skill.Group = new(domain.SkillsGroup)
			if err := skillsGroup.Uuid.AssignTo(&skill.Group.Uuid); err != nil {
				repo.logger.Warnf("Assign group uuid error: %s", err)
				return nil, errors.RepoError{
					Message: errors.AssignmentError,
					Details: err,
				}
			}
			if err := skillsGroup.Name.AssignTo(&skill.Group.Name); err != nil {
				repo.logger.Warnf("Assign group name error: %s", err)
				return nil, errors.RepoError{
					Message: errors.AssignmentError,
					Details: err,
				}
			}
		}
		if skillsGroup.Uuid.Status == pgtype.Present {
			skill.Programme = new(domain.SkillsProgramme)
			if err := skillsProgramme.Uuid.AssignTo(&skill.Programme.Uuid); err != nil {
				repo.logger.Warnf("Assign programme uuid error: %s", err)
				return nil, errors.RepoError{
					Message: errors.AssignmentError,
					Details: err,
				}
			}
			if err := skillsProgramme.Name.AssignTo(&skill.Programme.Name); err != nil {
				repo.logger.Warnf("Assign programme name error: %s", err)
				return nil, errors.RepoError{
					Message: errors.AssignmentError,
					Details: err,
				}
			}
		}
		skills = append(skills, skill)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("Rows error %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}
	return skills, err
}
