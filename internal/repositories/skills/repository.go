package skills

import (
	"context"
	"study-plans-service/internal/domain"
)

type Repository interface {
	CreateGroup(context.Context, *domain.SkillsGroup) error
	UpdateGroup(context.Context, *domain.SkillsGroup) error
	DeleteGroup(context.Context, domain.UuidString) error
	GetGroups(ctx context.Context, uuids []string) ([]domain.SkillsGroup, error)
	CreateProgramme(context.Context, *domain.SkillsProgramme) error
	UpdateProgramme(context.Context, *domain.SkillsProgramme) error
	DeleteProgramme(context.Context, domain.UuidString) error
	GetProgrammes(ctx context.Context, uuids []string) ([]domain.SkillsProgramme, error)

	Create(context.Context, *domain.Skill) error
	Update(context.Context, *domain.Skill) error
	Delete(context.Context, domain.UuidString) error
	Get(ctx context.Context, uuids []string) ([]domain.Skill, error)

	CreateCrossSkillsDisciplines(ctx context.Context, uuidSkill, uuidDiscipline string) error
	CreateCrossSkillsGroups(ctx context.Context, uuidSkill, uuidGroup string) error
	CreateCrossSkillsProgrammes(ctx context.Context, uuidSkill, uuidProgramme string) error

	DeleteCrossSkillsDisciplines(ctx context.Context, uuid domain.UuidString) error
	DeleteCrossSkillsGroups(ctx context.Context, uuid domain.UuidString) error
	DeleteCrossSkillsProgrammes(ctx context.Context, uuid domain.UuidString) error

	DeleteCrossSkillsGroupsByGroup(ctx context.Context, uuid domain.UuidString) error
	DeleteCrossSkillsProgrammesByProgramme(ctx context.Context, uuid domain.UuidString) error
}
