package skills

import "github.com/jackc/pgtype"

type SkillsProgramme struct {
	Uuid      pgtype.Varchar
	Name      pgtype.Varchar
	CreatedAt pgtype.Timestamp
	UpdatedAt pgtype.Timestamp
}
