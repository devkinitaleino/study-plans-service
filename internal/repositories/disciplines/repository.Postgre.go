package disciplines

import (
	"context"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.opentelemetry.io/otel"
	"strings"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/repositories"
	"study-plans-service/pkg/logger"
)

type RepositoryPostgre struct {
	pool   *pgxpool.Pool
	logger logger.Logger
}

const (
	GetDisciplinesByUuidsQuery      = `SELECT uuid, name, semester, created_at, updated_at FROM disciplines WHERE deleted_at IS NULL AND uuid IN `
	GetAllDisciplinesQuery          = `SELECT uuid, name, semester, created_at, updated_at FROM disciplines WHERE deleted_at IS NULL;`
	GetDisciplinesByUuidsSkillQuery = `SELECT  d.uuid, d.name, d.semester, d.created_at, d.updated_at, csd.uuid_skill FROM cross_skills_disciplines csd
													INNER JOIN disciplines d on csd.uuid_discipline = d.uuid
													WHERE csd.uuid_skill IN `
	CreateDisciplineQuery                      = `INSERT INTO disciplines (uuid, name, semester) VALUES ($1,$2,$3) RETURNING created_at, updated_at;`
	CreateCrossDisciplinesPresetsQuery         = `INSERT INTO cross_disciplines_presets (uuid, uuid_preset, uuid_discipline) VALUES ($1, $2, $3);`
	CreateCrossSkillsDisciplinesQuery          = `INSERT INTO cross_skills_disciplines (uuid, uuid_skill, uuid_discipline) VALUES ($1, $2, $3);`
	CreateCrossDisciplinesRealDisciplinesQuery = `INSERT INTO cross_disciplines_real_disciplines (uuid, uuid_real_discipline, uuid_discipline) VALUES ($1, $2, $3);`
	DeleteCrossDisciplinesPresetsQuery         = `DELETE FROM cross_disciplines_presets WHERE uuid_discipline = $1;`
	DeleteCrossSkillsDisciplinesQuery          = `DELETE FROM cross_skills_disciplines WHERE uuid_discipline = $1;`
	CrossDisciplinesRealDisciplinesQuery       = `DELETE FROM cross_disciplines_real_disciplines WHERE uuid_discipline = $1;`
	DeleteDisciplineQuery                      = `UPDATE disciplines SET deleted_at = now() WHERE uuid = $1;`
	UpdateDisciplineQuery                      = `UPDATE disciplines SET name = $2, semester = $3 WHERE uuid = $1 RETURNING created_at, updated_at;`
)

func NewRepository(pool *pgxpool.Pool, logger logger.Logger) Repository {
	return &RepositoryPostgre{
		pool:   pool,
		logger: logger,
	}
}

func (repo *RepositoryPostgre) Update(ctx context.Context, disc *domain.Discipline) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Update Discipline")
	defer childSpan.End()

	if err := repo.pool.QueryRow(ctx, UpdateDisciplineQuery, disc.Uuid, disc.Name, disc.Semester).Scan(
		&disc.CreatedAt,
		&disc.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("Can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Delete(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Discipline")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteDisciplineQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossDisciplinesPresets(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Discipline-Preset")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossDisciplinesPresetsQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossSkillsDisciplines(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Discipline-Preset")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeleteCrossSkillsDisciplinesQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) DeleteCrossDisciplinesRealDisciplines(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Cross Discipline-Preset")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CrossDisciplinesRealDisciplinesQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("Can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Create(ctx context.Context, disc *domain.Discipline) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Disciplines")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		CreateDisciplineQuery,
		disc.Uuid,
		disc.Name,
		disc.Semester,
	).Scan(
		&disc.CreatedAt,
		&disc.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("Can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossDisciplinesPresets(ctx context.Context, uuidPreset, uuidDiscipline string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Disciplines-Presets")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossDisciplinesPresetsQuery,
		uuid.New().String(),
		uuidPreset,
		uuidDiscipline,
	); err != nil {
		repo.logger.Warnf("Can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossSkillsDisciplines(ctx context.Context, uuidSkill, uuidDiscipline string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Skills-Disciplines")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossSkillsDisciplinesQuery,
		uuid.New().String(),
		uuidSkill,
		uuidDiscipline,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) CreateCrossDisciplinesRealDisciplines(ctx context.Context, uuidRealDiscipline, uuidDiscipline string) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Cross Disciplines-RealDisciplines")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		CreateCrossDisciplinesRealDisciplinesQuery,
		uuid.New().String(),
		uuidRealDiscipline,
		uuidDiscipline,
	); err != nil {
		repo.logger.Warnf("Can't exec: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Get(ctx context.Context, uuids []string) ([]domain.Discipline, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Get Disciplines")
	defer childSpan.End()

	rq := GetDisciplinesByUuidsQuery + "('" + strings.Join(uuids, "','") + "')" + ";"
	if len(uuids) == 0 {
		rq = GetAllDisciplinesQuery
	}

	rows, err := repo.pool.Query(ctx, rq)
	if err != nil {
		repo.logger.Warnf("Query error: %s", err)
		return nil, err
	}
	defer rows.Close()

	var disciplines []domain.Discipline
	for rows.Next() {
		var discipline domain.Discipline
		if err = rows.Scan(
			&discipline.Uuid,
			&discipline.Name,
			&discipline.Semester,
			&discipline.CreatedAt,
			&discipline.DeletedAt,
		); err != nil {
			repo.logger.Warnf("Scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}
		disciplines = append(disciplines, discipline)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("Rows error %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}
	return disciplines, err
}

func (repo *RepositoryPostgre) GetDisciplinesByUuidsSkill(ctx context.Context, uuids []string) (map[string][]*domain.Discipline, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Get Disciplines by uuids skill")
	defer childSpan.End()

	rows, err := repo.pool.Query(ctx, GetDisciplinesByUuidsSkillQuery+"('"+strings.Join(uuids, "','")+"')"+";")
	if err != nil {
		repo.logger.Warnf("Query error: %s", err)
		return nil, err
	}
	defer rows.Close()

	disciplines := make(map[string][]*domain.Discipline, len(uuids))
	for rows.Next() {
		var discipline domain.Discipline
		var uuidSkill string
		if err = rows.Scan(
			&discipline.Uuid,
			&discipline.Name,
			&discipline.Semester,
			&discipline.CreatedAt,
			&discipline.DeletedAt,
			&uuidSkill,
		); err != nil {
			repo.logger.Warnf("Scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}
		disciplines[uuidSkill] = append(disciplines[uuidSkill], &discipline)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("Rows error %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}
	return disciplines, err
}
