package disciplines

import (
	"context"
	"study-plans-service/internal/domain"
)

type Repository interface {
	Get(ctx context.Context, uuids []string) ([]domain.Discipline, error)
	GetDisciplinesByUuidsSkill(ctx context.Context, uuids []string) (map[string][]*domain.Discipline, error)
	Create(ctx context.Context, disc *domain.Discipline) error
	Delete(ctx context.Context, uuid domain.UuidString) error
	Update(ctx context.Context, disc *domain.Discipline) error

	CreateCrossDisciplinesPresets(ctx context.Context, uuidPreset, uuidDiscipline string) error
	CreateCrossSkillsDisciplines(ctx context.Context, uuidSkill, uuidDiscipline string) error
	CreateCrossDisciplinesRealDisciplines(ctx context.Context, uuidRealDiscipline, uuidDiscipline string) error

	DeleteCrossDisciplinesPresets(ctx context.Context, uuid domain.UuidString) error
	DeleteCrossSkillsDisciplines(ctx context.Context, uuid domain.UuidString) error
	DeleteCrossDisciplinesRealDisciplines(ctx context.Context, uuid domain.UuidString) error
}
