package repositories

import (
	"context"
	"study-plans-service/internal/domain"
)

type PlansRepository interface {
	Create(ctx context.Context, plan *domain.Plan) error
	Update(ctx context.Context, plan *domain.Plan) error
	Delete(ctx context.Context, uuid domain.UuidString) error
	GetByUuidsGroup(ctx context.Context, uuidsGroup []string) ([]domain.Plan, error)
}
