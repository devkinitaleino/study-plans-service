package repositories

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.opentelemetry.io/otel"
	"strings"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/repositories"
	"study-plans-service/pkg/logger"
)

type RepositoryPostgre struct {
	pool   *pgxpool.Pool
	logger logger.Logger
}

const (
	CreatePlanQuery             = `INSERT INTO plans (uuid, uuid_group) VALUES ($1,$2) RETURNING created_at, updated_at;`
	UpdatePlanQuery             = `UPDATE plans SET uuid_group = $1 WHERE uuid = $2 RETURNING created_at, updated_at;`
	DeletePlanQuery             = `UPDATE plans SET deleted_at = now() WHERE uuid = $1;`
	SelectPlansByUuidGroupQuery = `SELECT uuid, uuid_group, created_at, updated_at FROM plans WHERE deleted_at IS NULL AND uuid_group IN `
)

func NewRepository(pool *pgxpool.Pool, logger logger.Logger) PlansRepository {
	return &RepositoryPostgre{
		pool:   pool,
		logger: logger,
	}
}

func (repo *RepositoryPostgre) Create(ctx context.Context, plan *domain.Plan) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Create Plan")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		CreatePlanQuery,
		plan.Uuid,
		plan.UuidGroup,
	).Scan(
		&plan.CreatedAt,
		&plan.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Update(ctx context.Context, plan *domain.Plan) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Update Plan")
	defer childSpan.End()

	if err := repo.pool.QueryRow(
		ctx,
		UpdatePlanQuery,
		plan.UuidGroup,
		plan.Uuid,
	).Scan(
		&plan.CreatedAt,
		&plan.UpdatedAt,
	); err != nil {
		repo.logger.Warnf("can't scan: %s", err)
		return errors.RepoError{
			Message: errors.ScanError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) Delete(ctx context.Context, uuid domain.UuidString) error {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - Delete Plan")
	defer childSpan.End()

	if _, err := repo.pool.Exec(
		ctx,
		DeletePlanQuery,
		uuid,
	); err != nil {
		repo.logger.Warnf("can't execute: %s", err)
		return errors.RepoError{
			Message: errors.ExecutionError,
			Details: err,
		}
	}
	return nil
}

func (repo *RepositoryPostgre) GetByUuidsGroup(ctx context.Context, uuidsGroup []string) ([]domain.Plan, error) {
	tr := otel.Tracer("")
	_, childSpan := tr.Start(ctx, "DB query - GetPlans")
	defer childSpan.End()

	rows, err := repo.pool.Query(ctx, SelectPlansByUuidGroupQuery+"('"+strings.Join(uuidsGroup, "','")+"')"+";")
	if err != nil {
		repo.logger.Warnf("query error: %s", err)
		return nil, err
	}

	defer func() {
		rows.Close()
	}()

	var plans []domain.Plan

	for rows.Next() {
		var plan domain.Plan
		if err = rows.Scan(
			&plan.Uuid,
			&plan.UuidGroup,
			&plan.CreatedAt,
			&plan.UpdatedAt,
		); err != nil {
			repo.logger.Warnf("scan error: %s", err)
			return nil, errors.RepoError{
				Message: errors.ScanError,
				Details: err,
			}
		}

		plans = append(plans, plan)
	}

	if err = rows.Err(); err != nil {
		repo.logger.Warnf("rows error: %s", err)
		return nil, errors.RepoError{
			Message: errors.RowsError,
			Details: err,
		}
	}

	return plans, nil
}
