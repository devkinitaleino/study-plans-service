package app

import (
	"context"
	middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/redis/go-redis/v9"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
	"os/signal"
	"study-plans-service/config"
	"study-plans-service/internal/client"
	delivery "study-plans-service/internal/delivery/grpc"
	"study-plans-service/pkg/interceptors"
	"study-plans-service/pkg/logger"
	"study-plans-service/pkg/postgres"
	redisClient "study-plans-service/pkg/redis"
	"study-plans-service/pkg/tracer"
	plans "study-plans-service/proto"
	"syscall"
	"time"
)

const (
	maxConnectionIdle = 5
	gRPCTimeout       = 15
	maxConnectionAge  = 5
	gRPCTime          = 10
)

type App struct {
	logger   logger.Logger
	config   *config.Config
	postgres *pgxpool.Pool
	grpc     *delivery.GrpcController
	im       interceptors.InterceptorManager
}

func NewApp(logger logger.Logger, config *config.Config) *App {
	return &App{logger: logger, config: config}
}

func (app *App) Run() error {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	redClient := redisClient.NewRedis(redis.NewClient(&redis.Options{
		Addr:     app.config.Redis.Host,
		Username: app.config.Redis.User,
		Password: app.config.Redis.Password,
	}))

	app.im = interceptors.NewInterceptorManager(app.logger, redClient)

	pgxConn, err := postgres.NewPgxConn(&app.config.Postgres)
	if err != nil {
		return errors.Wrap(err, "postgresql.NewPgxConn")
	}

	app.postgres = pgxConn
	app.logger.Infof("postgres connected: %v", pgxConn.Stat().TotalConns())
	defer pgxConn.Close()

	tracerProvider, err := tracer.NewTracerWithJaegerExporter(app.config.Jaeger, app.config.ServiceName)
	otel.SetTracerProvider(tracerProvider)

	cl := client.NewClient(app.config, app.logger)

	app.grpc = delivery.NewGrpcController(app.postgres, app.logger, cl)

	closeGrpcServer, grpcServer, err := app.newStudyPlansGrpcServer()
	if err != nil {
		return errors.Wrap(err, "NewLectureGrpcServer")
	}
	defer closeGrpcServer() // nolint: errcheck

	<-ctx.Done()
	grpcServer.GracefulStop()

	return nil
}

func (app *App) newStudyPlansGrpcServer() (func() error, *grpc.Server, error) {
	l, err := net.Listen("tcp", app.config.Grpc.Address)
	if err != nil {
		return nil, nil, errors.Wrap(err, "net.Listen")
	}

	grpcServer := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{
			MaxConnectionIdle: maxConnectionIdle * time.Minute,
			Timeout:           gRPCTimeout * time.Second,
			MaxConnectionAge:  maxConnectionAge * time.Minute,
			Time:              gRPCTime * time.Minute,
		}),
		grpc.UnaryInterceptor(middleware.ChainUnaryServer(
			ctxtags.UnaryServerInterceptor(),
			opentracing.UnaryServerInterceptor(),
			prometheus.UnaryServerInterceptor,
			recovery.UnaryServerInterceptor(),
			otelgrpc.UnaryServerInterceptor(),
			app.im.Logger,
			app.im.Cache,
		),
		),
		grpc.StreamInterceptor(otelgrpc.StreamServerInterceptor()),
		grpc.MaxMsgSize(100*1024*1024),
		grpc.MaxRecvMsgSize(100*1024*1024),
		grpc.MaxSendMsgSize(100*1024*1024),
	)

	plans.RegisterPlansServiceServer(grpcServer, app.grpc)

	if app.config.Grpc.Development {
		reflection.Register(grpcServer)
	}

	go func() {
		app.logger.Infof("%s is listening on port: %s", app.config.ServiceName, app.config.Grpc.Address)
		app.logger.Fatal(grpcServer.Serve(l))
	}()

	return l.Close, grpcServer, nil
}
