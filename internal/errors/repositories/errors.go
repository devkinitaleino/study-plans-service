package errors

import "fmt"

const (
	ScanErrCode = iota
	ExecErrCode
	RowsErrCode
	AssignmentErrCode
)

var (
	ScanError       = fmt.Sprintf("can not scan, code %d", ScanErrCode)
	ExecutionError  = fmt.Sprintf("can not execute, code %d", ExecErrCode)
	RowsError       = fmt.Sprintf("error in rows, code %d", RowsErrCode)
	AssignmentError = fmt.Sprintf("can not assign, code %d", AssignmentErrCode)
)

type RepoError struct {
	Message string
	Details any
}

func (repoError RepoError) Error() string {
	return fmt.Sprintf("message: %s, details: %v", repoError.Message, repoError.Details)
}
