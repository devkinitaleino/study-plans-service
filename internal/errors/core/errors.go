package errors

import (
	"fmt"
)

const (
	UnknownTypeOfProtoMessageErrCode = iota
	DataErrCode
)

var (
	UnknownTypeOfProtoMessageError = fmt.Errorf("unknown type of proto message error, code %d", UnknownTypeOfProtoMessageErrCode)
	DataError                      = fmt.Sprintf("error from data source, code %d", DataErrCode)
)

type CoreError struct {
	Message string
	Details any
}

func (coreError CoreError) Error() string {
	return fmt.Sprintf("message: %s, details: %v", coreError.Message, coreError.Details)
}
