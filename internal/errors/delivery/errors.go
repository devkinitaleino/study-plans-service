package errors

import (
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeliveryError struct {
	GrpcCode codes.Code
	Message  string
	Details  any
}

func (deliveryError DeliveryError) Error() string {
	return fmt.Sprintf("message: %s, details: %v", deliveryError.Message, deliveryError.Details)
}

func RaiseError(err DeliveryError) error {
	return status.Error(err.GrpcCode, err.Error())
}
