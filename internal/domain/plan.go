package domain

import (
	"google.golang.org/protobuf/proto"
	errors "study-plans-service/internal/errors/core"
	plans "study-plans-service/proto"
	"time"
)

type Plan struct {
	Uuid      UuidString
	UuidGroup UuidString
	CreatedAt time.Time
	UpdatedAt time.Time
}

func NewPlanFromProto(message proto.Message) (*Plan, error) {
	switch m := message.(type) {
	case *plans.CreatePlanRequestMessage:
		return &Plan{
			Uuid:      NewUuidString(),
			UuidGroup: NewUuidFromString(m.UuidGroup),
		}, nil
	case *plans.UpdatePlanRequestMessage:
		return &Plan{
			Uuid:      NewUuidFromString(m.Uuid),
			UuidGroup: NewUuidFromString(m.UuidGroup),
		}, nil
	default:
		return nil, errors.UnknownTypeOfProtoMessageError
	}
}
