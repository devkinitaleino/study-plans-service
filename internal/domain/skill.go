package domain

import (
	"google.golang.org/protobuf/proto"
	errors "study-plans-service/internal/errors/core"
	plans "study-plans-service/proto"
	"time"
)

type Skill struct {
	// main properties from table
	Uuid      UuidString
	Name      string
	Content   Html
	Picture   UuidString
	CreatedAt time.Time
	UpdatedAt time.Time

	// related properties
	Group       *SkillsGroup
	Programme   *SkillsProgramme
	Disciplines []*Discipline
}

func NewSkillFromProto(message proto.Message) (*Skill, error) {
	switch m := message.(type) {
	case *plans.CreateSkillRequestMessage:
		skill := Skill{
			Uuid:      NewUuidString(),
			Name:      m.Name,
			Content:   NewHtmlFromString(m.Content),
			Picture:   NewUuidFromString(m.UuidPicture),
			Group:     nil,
			Programme: nil,
		}
		if m.UuidGroup != "" {
			skill.Group = new(SkillsGroup)
			skill.Group.Uuid = NewUuidFromString(m.UuidGroup)
		}
		if m.UuidProgramme != "" {
			skill.Programme = new(SkillsProgramme)
			skill.Programme.Uuid = NewUuidFromString(m.UuidProgramme)
		}
		if len(m.UuidsDiscipline) != 0 {
			for _, uuid := range m.UuidsDiscipline {
				skill.Disciplines = append(skill.Disciplines, &Discipline{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		return &skill, nil
	case *plans.UpdateSkillRequestMessage:
		skill := Skill{
			Uuid:      NewUuidFromString(m.Uuid),
			Name:      m.Name,
			Content:   NewHtmlFromString(m.Content),
			Picture:   NewUuidFromString(m.UuidPicture),
			Group:     nil,
			Programme: nil,
		}
		if m.UuidGroup != "" {
			skill.Group = new(SkillsGroup)
			skill.Group.Uuid = NewUuidFromString(m.UuidGroup)
		}
		if m.UuidProgramme != "" {
			skill.Programme = new(SkillsProgramme)
			skill.Programme.Uuid = NewUuidFromString(m.UuidProgramme)
		}
		if len(m.UuidsDiscipline) != 0 {
			for _, uuid := range m.UuidsDiscipline {
				skill.Disciplines = append(skill.Disciplines, &Discipline{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		return &skill, nil
	default:
		return nil, errors.UnknownTypeOfProtoMessageError
	}
}
