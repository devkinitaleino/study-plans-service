package domain

import (
	"google.golang.org/protobuf/proto"
	errors "study-plans-service/internal/errors/core"
	plans "study-plans-service/proto"
	"time"
)

type SkillsProgramme struct {
	Uuid      UuidString
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func NewSkillsProgrammeFromProto(message proto.Message) (*SkillsProgramme, error) {
	switch m := message.(type) {
	case *plans.CreateSkillsProgrammeRequestMessage:
		return &SkillsProgramme{
			Uuid: NewUuidString(),
			Name: m.Name,
		}, nil
	case *plans.UpdateSkillsProgrammeRequestMessage:
		return &SkillsProgramme{
			Uuid: NewUuidFromString(m.Uuid),
			Name: m.Name,
		}, nil
	default:
		return nil, errors.UnknownTypeOfProtoMessageError
	}
}
