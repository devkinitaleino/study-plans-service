package presets

import (
	"context"
	"study-plans-service/internal/client"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/core"
	repositories "study-plans-service/internal/repositories/presets"
	"study-plans-service/pkg/logger"
)

type Presets struct {
	repository repositories.Repository
	logger     logger.Logger
	client     *client.Client
}

func NewPresets(logger logger.Logger, client *client.Client, repository repositories.Repository) *Presets {
	return &Presets{
		repository: repository,
		logger:     logger,
		client:     client,
	}
}

func (core *Presets) Create(ctx context.Context, domain *domain.Presets) error {
	if err := core.repository.Create(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if domain.Groups != nil {
		for _, uuidGroup := range domain.Groups {
			if err := core.repository.CreateCrossPresetsGroups(ctx, domain.Uuid.ToString(), uuidGroup.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	if domain.Disciplines != nil {
		for _, discipline := range domain.Disciplines {
			if err := core.repository.CreateCrossDisciplinesPresets(ctx, domain.Uuid.ToString(), discipline.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	return nil
}

func (core *Presets) Delete(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.Delete(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.deleteCrosses(ctx, uuid); err != nil {
		return err
	}
	return nil
}

func (core *Presets) Update(ctx context.Context, domain *domain.Presets) error {
	if err := core.repository.Update(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.deleteCrosses(ctx, domain.Uuid); err != nil {
		return err
	}
	if domain.Groups != nil {
		for _, uuidGroup := range domain.Groups {
			if err := core.repository.CreateCrossPresetsGroups(ctx, domain.Uuid.ToString(), uuidGroup.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	if domain.Disciplines != nil {
		for _, discipline := range domain.Disciplines {
			if err := core.repository.CreateCrossDisciplinesPresets(ctx, domain.Uuid.ToString(), discipline.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	return nil
}

func (core *Presets) Get(ctx context.Context, uuids []string) ([]domain.Presets, error) {
	disciplines, err := core.repository.Get(ctx, uuids)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return disciplines, nil
}

func (core *Presets) deleteCrosses(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.DeleteCrossPresetsGroups(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossDisciplinesPresets(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}
