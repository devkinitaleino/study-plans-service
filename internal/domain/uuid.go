package domain

import "github.com/google/uuid"

type UuidString string

func NewUuidFromString(value string) UuidString {
	_, err := uuid.Parse(value)
	if err != nil {
		return ""
	}
	return UuidString(value)
}

func NewUuidString() UuidString {
	return UuidString(uuid.New().String())
}

func (uuid UuidString) ToString() string {
	return string(uuid)
}
