package skills

import (
	"context"
	"study-plans-service/internal/client"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/core"
	"study-plans-service/internal/repositories/disciplines"
	"study-plans-service/internal/repositories/skills"
	"study-plans-service/pkg/logger"
)

type Skills struct {
	repository      skills.Repository
	disciplinesRepo disciplines.Repository
	logger          logger.Logger
	client          *client.Client
}

func NewSkills(logger logger.Logger, client *client.Client, repository skills.Repository, disciplinesRepo disciplines.Repository) *Skills {
	return &Skills{
		repository:      repository,
		disciplinesRepo: disciplinesRepo,
		logger:          logger,
		client:          client,
	}
}

func (core *Skills) Create(ctx context.Context, domain *domain.Skill) error {
	if err := core.repository.Create(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}

	if domain.Programme != nil {
		if err := core.repository.CreateCrossSkillsProgrammes(ctx, domain.Uuid.ToString(), domain.Programme.Uuid.ToString()); err != nil {
			return errors.CoreError{
				Message: errors.DataError,
				Details: err,
			}
		}
	}
	if domain.Group != nil {
		if err := core.repository.CreateCrossSkillsGroups(ctx, domain.Uuid.ToString(), domain.Group.Uuid.ToString()); err != nil {
			return errors.CoreError{
				Message: errors.DataError,
				Details: err,
			}
		}
	}
	if len(domain.Disciplines) != 0 {
		for _, discipline := range domain.Disciplines {
			if err := core.repository.CreateCrossSkillsDisciplines(ctx, domain.Uuid.ToString(), discipline.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}

	return nil
}

func (core *Skills) Update(ctx context.Context, domain *domain.Skill) error {
	if err := core.repository.Update(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}

	if err := core.deleteCrosses(ctx, domain.Uuid); err != nil {
		return err
	}

	if domain.Programme != nil {
		if err := core.repository.CreateCrossSkillsProgrammes(ctx, domain.Uuid.ToString(), domain.Programme.Uuid.ToString()); err != nil {
			return errors.CoreError{
				Message: errors.DataError,
				Details: err,
			}
		}
	}
	if domain.Group != nil {
		if err := core.repository.CreateCrossSkillsGroups(ctx, domain.Uuid.ToString(), domain.Group.Uuid.ToString()); err != nil {
			return errors.CoreError{
				Message: errors.DataError,
				Details: err,
			}
		}
	}
	if len(domain.Disciplines) != 0 {
		for _, discipline := range domain.Disciplines {
			if err := core.repository.CreateCrossSkillsDisciplines(ctx, domain.Uuid.ToString(), discipline.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}

	return nil
}

func (core *Skills) Delete(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.Delete(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.deleteCrosses(ctx, uuid); err != nil {
		return err
	}
	return nil
}

func (core *Skills) Get(ctx context.Context, uuids []string) ([]domain.Skill, error) {
	domainSkills, err := core.repository.Get(ctx, uuids)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	uuidsSkill := make([]string, 0, len(domainSkills))
	for _, skill := range domainSkills {
		uuidsSkill = append(uuidsSkill, skill.Uuid.ToString())
	}
	discs, err := core.disciplinesRepo.GetDisciplinesByUuidsSkill(ctx, uuidsSkill)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	for i, skill := range domainSkills {
		discsForSkill, ok := discs[skill.Uuid.ToString()]
		if ok {
			domainSkills[i].Disciplines = discsForSkill
		}
	}
	return domainSkills, nil
}

func (core *Skills) CreateGroup(ctx context.Context, domain *domain.SkillsGroup) error {
	if err := core.repository.CreateGroup(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Skills) UpdateGroup(ctx context.Context, domain *domain.SkillsGroup) error {
	if err := core.repository.UpdateGroup(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Skills) DeleteGroup(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.DeleteGroup(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossSkillsGroupsByGroup(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Skills) GetGroups(ctx context.Context, uuids []string) ([]domain.SkillsGroup, error) {
	groups, err := core.repository.GetGroups(ctx, uuids)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return groups, nil
}

func (core *Skills) CreateProgramme(ctx context.Context, domain *domain.SkillsProgramme) error {
	if err := core.repository.CreateProgramme(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Skills) UpdateProgramme(ctx context.Context, domain *domain.SkillsProgramme) error {
	if err := core.repository.UpdateProgramme(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Skills) DeleteProgramme(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.DeleteProgramme(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossSkillsProgrammesByProgramme(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Skills) GetProgrammes(ctx context.Context, uuids []string) ([]domain.SkillsProgramme, error) {
	programmes, err := core.repository.GetProgrammes(ctx, uuids)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return programmes, nil
}

func (core *Skills) deleteCrosses(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.DeleteCrossSkillsDisciplines(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossSkillsGroups(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossSkillsProgrammes(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}
