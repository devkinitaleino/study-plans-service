package domain

import (
	"google.golang.org/protobuf/proto"
	errors "study-plans-service/internal/errors/core"
	plans "study-plans-service/proto"
	"time"
)

type SkillsGroup struct {
	Uuid      UuidString
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func NewSkillsGroupFromProto(message proto.Message) (*SkillsGroup, error) {
	switch m := message.(type) {
	case *plans.CreateSkillsGroupRequestMessage:
		return &SkillsGroup{
			Uuid: NewUuidString(),
			Name: m.Name,
		}, nil
	case *plans.UpdateSkillsGroupRequestMessage:
		return &SkillsGroup{
			Uuid: NewUuidFromString(m.Uuid),
			Name: m.Name,
		}, nil
	default:
		return nil, errors.UnknownTypeOfProtoMessageError
	}
}
