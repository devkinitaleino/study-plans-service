package domain

type Html string

func NewHtmlFromString(value string) Html {
	return Html(value)
}

func (html Html) ToString() string {
	return string(html)
}
