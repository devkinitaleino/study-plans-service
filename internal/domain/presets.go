package domain

import (
	"google.golang.org/protobuf/proto"
	errors "study-plans-service/internal/errors/core"
	plans "study-plans-service/proto"
	"time"
)

type Presets struct {
	// main properties from table
	Uuid      UuidString
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time

	// related properties
	Groups      []UuidString
	Disciplines []*Discipline
}

func NewPresetFromProto(message proto.Message) (*Presets, error) {
	switch m := message.(type) {
	case *plans.CreatePresetRequestMessage:
		pres := Presets{
			Uuid: NewUuidString(),
			Name: m.Name,
		}
		if len(m.UuidsGroup) != 0 {
			pres.Groups = make([]UuidString, 0, len(m.UuidsGroup))
			for _, uuid := range m.UuidsGroup {
				pres.Groups = append(pres.Groups, NewUuidFromString(uuid))
			}
		}
		if len(m.UuidsDiscipline) != 0 {
			pres.Disciplines = make([]*Discipline, 0, len(m.UuidsDiscipline))
			for _, uuid := range m.UuidsDiscipline {
				pres.Disciplines = append(pres.Disciplines, &Discipline{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		return &pres, nil
	case *plans.UpdatePresetRequestMessage:
		pres := Presets{
			Uuid: NewUuidFromString(m.Uuid),
			Name: m.Name,
		}
		if len(m.UuidsGroup) != 0 {
			pres.Groups = make([]UuidString, 0, len(m.UuidsGroup))
			for _, uuid := range m.UuidsGroup {
				pres.Groups = append(pres.Groups, NewUuidFromString(uuid))
			}
		}
		if len(m.UuidsDiscipline) != 0 {
			pres.Disciplines = make([]*Discipline, 0, len(m.UuidsDiscipline))
			for _, uuid := range m.UuidsDiscipline {
				pres.Disciplines = append(pres.Disciplines, &Discipline{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		return &pres, nil
	default:
		return nil, errors.UnknownTypeOfProtoMessageError
	}
}
