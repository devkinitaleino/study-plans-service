package disciplines

import (
	"context"
	"study-plans-service/internal/client"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/core"
	repositories "study-plans-service/internal/repositories/disciplines"
	"study-plans-service/pkg/logger"
)

type Disciplines struct {
	repository repositories.Repository
	logger     logger.Logger
	client     *client.Client
}

func NewDisciplines(logger logger.Logger, client *client.Client, repository repositories.Repository) *Disciplines {
	return &Disciplines{
		repository: repository,
		logger:     logger,
		client:     client,
	}
}

func (core *Disciplines) Update(ctx context.Context, domain *domain.Discipline) error {
	if err := core.repository.Update(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.deleteCrosses(ctx, domain.Uuid); err != nil {
		return err
	}
	if domain.Presets != nil {
		for _, preset := range domain.Presets {
			if err := core.repository.CreateCrossDisciplinesPresets(ctx, preset.Uuid.ToString(), domain.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	if domain.Skills != nil {
		for _, skill := range domain.Skills {
			if err := core.repository.CreateCrossSkillsDisciplines(ctx, skill.Uuid.ToString(), domain.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	if domain.RealDisciplines != nil {
		for _, uuid := range domain.RealDisciplines {
			if err := core.repository.CreateCrossDisciplinesRealDisciplines(ctx, uuid.ToString(), domain.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	return nil
}

func (core *Disciplines) Delete(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.Delete(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.deleteCrosses(ctx, uuid); err != nil {
		return err
	}
	return nil
}

func (core *Disciplines) Create(ctx context.Context, domain *domain.Discipline) error {
	if err := core.repository.Create(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if domain.Presets != nil {
		for _, preset := range domain.Presets {
			if err := core.repository.CreateCrossDisciplinesPresets(ctx, preset.Uuid.ToString(), domain.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	if domain.Skills != nil {
		for _, skill := range domain.Skills {
			if err := core.repository.CreateCrossSkillsDisciplines(ctx, skill.Uuid.ToString(), domain.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	if domain.RealDisciplines != nil {
		for _, uuid := range domain.RealDisciplines {
			if err := core.repository.CreateCrossDisciplinesRealDisciplines(ctx, uuid.ToString(), domain.Uuid.ToString()); err != nil {
				return errors.CoreError{
					Message: errors.DataError,
					Details: err,
				}
			}
		}
	}
	return nil
}

func (core *Disciplines) GetDisciplines(ctx context.Context, uuids []string) ([]domain.Discipline, error) {
	disciplines, err := core.repository.Get(ctx, uuids)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return disciplines, nil
}

func (core *Disciplines) deleteCrosses(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.DeleteCrossDisciplinesPresets(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossSkillsDisciplines(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	if err := core.repository.DeleteCrossDisciplinesRealDisciplines(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}

	return nil
}
