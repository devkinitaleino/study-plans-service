package domain

import (
	"google.golang.org/protobuf/proto"
	errors "study-plans-service/internal/errors/core"
	plans "study-plans-service/proto"
	"time"
)

type Discipline struct {
	// main properties from table
	Uuid      UuidString
	Name      string
	Semester  string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time

	// related properties
	Presets         []*Presets
	RealDisciplines []UuidString
	Skills          []*Skill
}

func NewDisciplineFromProto(message proto.Message) (*Discipline, error) {
	switch m := message.(type) {
	case *plans.CreateDisciplineRequestMessage:
		disc := Discipline{
			Uuid:     NewUuidString(),
			Name:     m.Name,
			Semester: m.Semester,
		}
		if len(m.UuidsPresets) != 0 {
			disc.Presets = make([]*Presets, 0, len(m.UuidsPresets))
			for _, uuid := range m.UuidsPresets {
				disc.Presets = append(disc.Presets, &Presets{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		if len(m.UuidsRealDisciplines) != 0 {
			disc.RealDisciplines = make([]UuidString, 0, len(m.UuidsRealDisciplines))
			for _, uuid := range m.UuidsRealDisciplines {
				disc.RealDisciplines = append(disc.RealDisciplines, NewUuidFromString(uuid))
			}
		}
		if len(m.UuidsSkills) != 0 {
			disc.Skills = make([]*Skill, 0, len(m.UuidsSkills))
			for _, uuid := range m.UuidsSkills {
				disc.Skills = append(disc.Skills, &Skill{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		return &disc, nil
	case *plans.UpdateDisciplineRequestMessage:
		disc := Discipline{
			Uuid:     NewUuidFromString(m.Uuid),
			Name:     m.Name,
			Semester: m.Semester,
		}
		if len(m.UuidsPresets) != 0 {
			disc.Presets = make([]*Presets, 0, len(m.UuidsPresets))
			for _, uuid := range m.UuidsPresets {
				disc.Presets = append(disc.Presets, &Presets{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		if len(m.UuidsRealDisciplines) != 0 {
			disc.RealDisciplines = make([]UuidString, 0, len(m.UuidsRealDisciplines))
			for _, uuid := range m.UuidsRealDisciplines {
				disc.RealDisciplines = append(disc.RealDisciplines, NewUuidFromString(uuid))
			}
		}
		if len(m.UuidsSkills) != 0 {
			disc.Skills = make([]*Skill, 0, len(m.UuidsSkills))
			for _, uuid := range m.UuidsSkills {
				disc.Skills = append(disc.Skills, &Skill{
					Uuid: NewUuidFromString(uuid),
				})
			}
		}
		return &disc, nil
	default:
		return nil, errors.UnknownTypeOfProtoMessageError
	}
}
