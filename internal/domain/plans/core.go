package plans

import (
	"context"
	"study-plans-service/internal/client"
	"study-plans-service/internal/domain"
	errors "study-plans-service/internal/errors/core"
	repositories "study-plans-service/internal/repositories/plans"
	"study-plans-service/pkg/logger"
)

type Plans struct {
	repository repositories.PlansRepository
	logger     logger.Logger
	client     *client.Client
}

func NewPlans(logger logger.Logger, client *client.Client, repository repositories.PlansRepository) *Plans {
	return &Plans{
		repository: repository,
		logger:     logger,
		client:     client,
	}
}

func (core *Plans) Create(ctx context.Context, domain *domain.Plan) error {
	if err := core.repository.Create(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Plans) Update(ctx context.Context, domain *domain.Plan) error {
	if err := core.repository.Update(ctx, domain); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Plans) Delete(ctx context.Context, uuid domain.UuidString) error {
	if err := core.repository.Delete(ctx, uuid); err != nil {
		return errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}
	return nil
}

func (core *Plans) GetByUuidsGroup(ctx context.Context, uuids []string) ([]domain.Plan, error) {
	plans, err := core.repository.GetByUuidsGroup(ctx, uuids)
	if err != nil {
		return nil, errors.CoreError{
			Message: errors.DataError,
			Details: err,
		}
	}

	return plans, nil
}
