package main

import (
	"log"
	"study-plans-service/config"
	"study-plans-service/internal/app"
	"study-plans-service/pkg/logger"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	appLogger := logger.NewAppLogger(&cfg.Logger)
	appLogger.InitLogger()
	appLogger.WithName(cfg.ServiceName)

	s := app.NewApp(appLogger, cfg)
	appLogger.Fatal(s.Run())
}
