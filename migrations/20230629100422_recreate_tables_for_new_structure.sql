-- +goose Up
-- +goose StatementBegin
DROP INDEX IF EXISTS uidx_for_plans_on_uuid_group;
DROP INDEX IF EXISTS idx_for_cross_d_p_on_uuid_plan;
DROP INDEX IF EXISTS idx_for_cross_d_p_on_uuid_discipline;
DROP TABLE IF EXISTS cross_disciplines_plans;
DROP TABLE IF EXISTS plans;
DROP TABLE IF EXISTS disciplines;

CREATE TABLE IF NOT EXISTS skills (
    uuid varchar(36) primary key,
    name text NOT NULL,
    content text,
    picture varchar(36) NOT NULL,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE TABLE IF NOT EXISTS skills_programmes (
    uuid varchar(36) primary key,
    name text NOT NULL,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE TABLE IF NOT EXISTS skills_groups (
    uuid varchar(36) primary key,
    name text NOT NULL,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE TABLE IF NOT EXISTS disciplines (
    uuid varchar(36) primary key,
    name text NOT NULL,
    semester text NOT NULL,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE TABLE IF NOT EXISTS cross_skills_programmes (
    uuid varchar(36) primary key,
    uuid_skill varchar(36) not null REFERENCES skills(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    uuid_programme varchar(36) not null REFERENCES skills_programmes(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE INDEX IF NOT EXISTS idx_for_cross_skills_programmes_on_uuid_skill ON cross_skills_programmes(uuid_skill);
CREATE INDEX IF NOT EXISTS idx_for_cross_skills_programmes_on_uuid_programme ON cross_skills_programmes(uuid_programme);

CREATE TABLE IF NOT EXISTS cross_skills_groups (
    uuid varchar(36) primary key,
    uuid_skill varchar(36) not null REFERENCES skills(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    uuid_group varchar(36) not null REFERENCES skills_groups(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE INDEX IF NOT EXISTS idx_for_cross_skills_groups_on_uuid_skill ON cross_skills_groups(uuid_skill);
CREATE INDEX IF NOT EXISTS idx_for_cross_skills_groups_on_uuid_group ON cross_skills_groups(uuid_group);

CREATE TABLE IF NOT EXISTS cross_skills_disciplines (
    uuid varchar(36) primary key,
    uuid_skill varchar(36) not null REFERENCES skills(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    uuid_discipline varchar(36) not null REFERENCES disciplines(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE INDEX IF NOT EXISTS idx_for_cross_skills_disciplines_on_uuid_skill ON cross_skills_disciplines(uuid_skill);
CREATE INDEX IF NOT EXISTS idx_for_cross_skills_disciplines_on_uuid_discipline ON cross_skills_disciplines(uuid_discipline);

CREATE TABLE IF NOT EXISTS cross_disciplines_real_disciplines (
    uuid varchar(36) primary key,
    uuid_real_discipline varchar(36) not null,
    uuid_discipline varchar(36) not null REFERENCES disciplines(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE INDEX IF NOT EXISTS idx_for_cross_disciplines_real_disciplines_on_uuid_real ON cross_disciplines_real_disciplines(uuid_real_discipline);
CREATE INDEX IF NOT EXISTS idx_for_cross_disciplines_real_disciplines_on_uuid_discipline ON cross_disciplines_real_disciplines(uuid_discipline);

CREATE TABLE IF NOT EXISTS presets (
    uuid varchar(36) primary key,
    name text NOT NULL,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE TABLE IF NOT EXISTS cross_disciplines_presets (
    uuid varchar(36) primary key,
    uuid_preset varchar(36) not null REFERENCES presets(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    uuid_discipline varchar(36) not null REFERENCES disciplines(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE INDEX IF NOT EXISTS idx_for_cross_disciplines_presets_on_uuid_preset ON cross_disciplines_presets(uuid_preset);
CREATE INDEX IF NOT EXISTS idx_for_cross_disciplines_presets_on_uuid_discipline ON cross_disciplines_presets(uuid_discipline);

CREATE TABLE IF NOT EXISTS cross_presets_groups (
    uuid varchar(36) primary key,
    uuid_preset varchar(36) not null REFERENCES presets(uuid) ON DELETE CASCADE ON UPDATE CASCADE,
    uuid_group varchar(36) not null,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE INDEX IF NOT EXISTS idx_for_cross_presets_groups_on_uuid_preset ON cross_presets_groups(uuid_preset);
CREATE INDEX IF NOT EXISTS idx_for_cross_presets_groups_on_uuid_group ON cross_presets_groups(uuid_group);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP INDEX IF EXISTS idx_for_cross_presets_groups_on_uuid_group;
DROP INDEX IF EXISTS idx_for_cross_presets_groups_on_uuid_preset;
DROP TABLE IF EXISTS cross_presets_groups;
DROP INDEX IF EXISTS idx_for_cross_disciplines_presets_on_uuid_preset;
DROP INDEX IF EXISTS idx_for_cross_disciplines_presets_on_uuid_discipline;
DROP TABLE IF EXISTS cross_disciplines_presets;
DROP TABLE IF EXISTS presets;
DROP INDEX IF EXISTS idx_for_cross_disciplines_real_disciplines_on_uuid_real;
DROP INDEX IF EXISTS idx_for_cross_disciplines_real_disciplines_on_uuid_discipline;
DROP TABLE IF EXISTS cross_disciplines_real_disciplines;
DROP INDEX IF EXISTS idx_for_cross_skills_disciplines_on_uuid_discipline;
DROP INDEX IF EXISTS idx_for_cross_skills_disciplines_on_uuid_skill;
DROP TABLE IF EXISTS cross_skills_disciplines;
DROP INDEX IF EXISTS idx_for_cross_skills_groups_on_uuid_group;
DROP INDEX IF EXISTS idx_for_cross_skills_groups_on_uuid_skill;
DROP TABLE IF EXISTS cross_skills_groups;
DROP INDEX IF EXISTS idx_for_cross_skills_programmes_on_uuid_programme;
DROP INDEX IF EXISTS idx_for_cross_skills_programmes_on_uuid_skill;
DROP TABLE IF EXISTS cross_skills_programmes;
DROP TABLE IF EXISTS skills_groups;
DROP TABLE IF EXISTS skills_programmes;
DROP TABLE IF EXISTS disciplines;
DROP TABLE IF EXISTS skills;
-- +goose StatementEnd
