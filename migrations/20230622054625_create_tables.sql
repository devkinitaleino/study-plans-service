-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS plans (
    uuid varchar(36) primary key,
    uuid_group varchar(36) not null,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);
CREATE UNIQUE INDEX IF NOT EXISTS uidx_for_plans_on_uuid_group ON plans(uuid_group);

CREATE TABLE IF NOT EXISTS disciplines (
    uuid varchar(36) primary key,
    name varchar(255),
    uuid_origin varchar(36) not null,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);

CREATE TABLE IF NOT EXISTS cross_disciplines_plans (
    uuid varchar(36) primary key,
    uuid_plan varchar(36) not null REFERENCES plans(uuid) ON DELETE CASCADE ON UPDATE RESTRICT,
    uuid_discipline varchar(36) not null REFERENCES disciplines(uuid) ON DELETE CASCADE ON UPDATE RESTRICT,
    created_at timestamp(0) not null default CURRENT_TIMESTAMP,
    updated_at timestamp(0) not null default CURRENT_TIMESTAMP,
    deleted_at timestamp(0)
);
CREATE INDEX IF NOT EXISTS idx_for_cross_d_p_on_uuid_plan ON cross_disciplines_plans(uuid_plan);
CREATE INDEX IF NOT EXISTS idx_for_cross_d_p_on_uuid_discipline ON cross_disciplines_plans(uuid_discipline);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP INDEX IF EXISTS uidx_for_plans_on_uuid_group;
DROP TABLE IF EXISTS plans;
DROP TABLE IF EXISTS disciplines;
DROP INDEX IF EXISTS idx_for_cross_d_p_on_uuid_plan;
DROP INDEX IF EXISTS idx_for_cross_d_p_on_uuid_discipline;
DROP TABLE IF EXISTS cross_disciplines_plans;
-- +goose StatementEnd
