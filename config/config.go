package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"study-plans-service/pkg/logger"
	"study-plans-service/pkg/postgres"
)

type Config struct {
	ServiceName string          `yaml:"serviceName" env:"SERVICE_NAME"`
	Grpc        GRPC            `yaml:"grpc"`
	Postgres    postgres.Config `yaml:"postgres"`
	Logger      logger.Config   `yaml:"logger"`
	Services    Services        `yaml:"services"`
	Jaeger      Jaeger          `yaml:"jaeger" env-prefix:"JAEGER_"`
	Redis       Redis           `yaml:"redis" env-prefix:"REDIS_"`
}

func NewConfig() (*Config, error) {
	var config Config
	if err := config.readConfig(); err != nil {
		return nil, err
	}
	return &config, nil
}

func (config *Config) readConfig() error {
	err := cleanenv.ReadConfig("./config/config.yaml", config)
	if err != nil {
		return err
	}
	return nil
}
