package config

type GRPC struct {
	Address     string `yaml:"address" env:"GRPC_ADDRESS"`
	Development bool   `yaml:"development" env:"GRPC_DEVELOPMENT"`
}
