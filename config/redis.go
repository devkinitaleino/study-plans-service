package config

type Redis struct {
	Host     string `yaml:"host" env:"HOST"`
	User     string `yaml:"user" env:"USER"`
	Password string `yaml:"password" env:"PASSWORD"`
}
