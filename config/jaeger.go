package config

type Jaeger struct {
	Host string `yaml:"host" env:"HOST"`
	Port string `yaml:"port" env:"PORT"`
}
