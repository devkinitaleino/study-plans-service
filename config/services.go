package config

type Services struct {
	Groups string `yaml:"groups" env:"GROUPS_SERVICE"`
	Auth   string `yaml:"auth" env:"AUTH_SERVICE"`
}
